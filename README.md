# DEVOPS4 - Front

## Lancer le projet

Se placer dans le fichier devops_front
Lancer le conteneur docker avec : 
	docker-compose up

## Rentrer dans l'API

Effectuer la commande suivante pour lister les conteneurs :
	docker ps
Récuperer le port du conteneur aprimault/sinatra:latest
Executer la commande suivante pour rentrer dans l'API : 
	docker exec -it portConteneurs bash