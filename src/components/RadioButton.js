import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from '@material-ui/core/FormGroup';
import { store } from "../stores/Store";
import { observer } from "mobx-react";


const styles = theme => ({
    root2: {
        display:"flex"
    },
    formControl: {
        margin: 0,
        display:"flex",
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
        display: "inline-block"
    }
});

class RadioButtonsGroup extends React.Component {
    state = {
        value: "pertinence"
    };

    handleChange = event => {
        this.setState({ value: event.target.value });
            store.triParDate = !store.triParDate
    };

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root2}>

                <FormGroup row>
                    <FormControl component="fieldset" className={classes.formControl}>
                        <RadioGroup
                            aria-label="gender"
                            name="gender2"
                            className={classes.group}
                            value={this.state.value}
                            onChange={this.handleChange}
                        >
                            <FormControlLabel
                                value="date"
                                control={<Radio color='secondary' />}
                                label="Date de parution"
                                labelPlacement="end"
                            />
                            <FormControlLabel
                                value="pertinence"
                                control={<Radio color="secondary" />}
                                label="Pertinence"
                                labelPlacement="end"
                            />
                        </RadioGroup>
                    </FormControl>
                </FormGroup>
            </div>
        );
    }
}

RadioButtonsGroup.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RadioButtonsGroup);
