import React from 'react';
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import PropTypes from "prop-types";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Historique from "./Historique";
import Tableau from "./TableauResultat";
import FiltresRecherches from "./FiltresRecherches";
import { store} from "../stores/Store";
import Login from "./Login";
import { observer } from "mobx-react";

const theme = createMuiTheme({
    palette: {
        primary: { main: '#2196F3' }, // Purple and green play nicely together.
        secondary: { main: '#5d5d5d' }, // This is just green.A700 as hex.
    },
    typography: { useNextVariants: true },
});

const drawerWidth = 240;


const styles = theme => ({
    root: {
        display: "flex"
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
    },
    toolbar: theme.mixins.toolbar
});

@observer
class App extends React.Component {
    render() {
        require("./styles.css");
        const { classes } = this.props;

        return (
            <div>
                    <MuiThemeProvider theme={theme}>

                            { store.current_user == null && (<Login/>) }

                            {store.current_user != null && (
                                <div className={classes.root}>
                                    <CssBaseline />
                                    <AppBar position="fixed" className={classes.appBar}>
                                <FiltresRecherches />
                            </AppBar>
                            <Historique/>
                            <main className={classes.content}>
                                <div className={classes.toolbar}/>
                                <Tableau/>
                            </main>
                                </div>
                                )}
                    </MuiThemeProvider>
            </div>
        );
    }
}

App.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(App);
