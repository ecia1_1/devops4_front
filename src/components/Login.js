import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { store } from "../stores/Store";
import { observer } from "mobx-react";

const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,

        backgroundColor: theme.palette.primary.main,

    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});


@observer
class Login extends React.Component {


    state = {
        username: '',
        password: '',
        loginMessage:''

    };

    handleLoginOpen = () => {
        this.setState(state => ({ openDialog: !state.openDialog }));
    };

    handleLogin = (e) => {
        fetch("http://localhost:9995/api/v1/login", {
            method: 'POST',
            headers: {
                'Accept': "*/*",
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            })

        }).then(response => {
            if (response.status !== 200) {
                this.setState({loginMessage: 'Nom d\'utilisateur ou mot de passe incorrect'});
            } else {
                this.handleLoginOpen();
                return response.json()
            }
        }).then(data => {
            if (data === undefined) {
                store.current_user = null;
            } else {
                data.name = data.name.split(' ')[0];
                store.current_user = data;
            }
        });
    };

    handleChange = (event) => {
        this.setState({username:event.target.value});

    };

    handleChangePass = (event) => {
        this.setState({password:event.target.value});
    };
    render() {


        const {classes} = this.props;

        return (
            <main className={classes.main}>
                <CssBaseline/>
                <Paper className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <Icon>import_contacts</Icon>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Connexion au Journal 2.0
                    </Typography>
                    <form className={classes.form}>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="username">Username</InputLabel>
                            <Input value={this.state.username} id="username" name="username" autoComplete="username" autoFocus onChange={this.handleChange}/>
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="password">Password</InputLabel>
                            <Input value={this.state.password} name="password" type="password" id="password" autoComplete="current-password" onChange={this.handleChangePass}/>
                        </FormControl>
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary"/>}
                            label="Remember me"
                        />
                        <Typography style={{color:'red'}}>{this.state.loginMessage}</Typography>
                        <Button
                            fullWidth
                            variant="contained"
                            color="secondary"
                            className={classes.submit}
                            onClick={this.handleLogin}
                        >Se connecter
                        </Button>
                    </form>
                </Paper>
            </main>
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);
