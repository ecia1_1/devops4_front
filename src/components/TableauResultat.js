import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import RadioButton from './RadioButton';
import Typography from '@material-ui/core/Typography';
import { store } from "../stores/Store";
import { observer } from "mobx-react";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import DateFnsUtils from '@date-io/date-fns';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';


import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Divider from "./FiltresRecherches";

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.secondary,
        color: theme.palette.common.black
    },
    body: {
        fontSize: 14
    }
}))(TableCell);

const CustomTableCell2 = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.secondary.dark,
        color: theme.palette.common.white
    },
    body: {
        fontSize: 14
    }
}))(TableCell);

const actionsStyles = theme => ({
    root: {
        flexShrink: 0,
        color: theme.palette.text.secondary,
        marginLeft: theme.spacing.unit * 2.5
    }
});


class TablePaginationActions extends React.Component {
    handleFirstPageButtonClick = event => {
        this.props.onChangePage(event, 0);
    };

    handleBackButtonClick = event => {
        this.props.onChangePage(event, this.props.page - 1);
    };

    handleNextButtonClick = event => {
        this.props.onChangePage(event, this.props.page + 1);
    };

    handleLastPageButtonClick = event => {
        this.props.onChangePage(
            event,
            Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1)
        );
    };

    render() {
        const { classes, count, page, rowsPerPage, theme } = this.props;

        return (
            <div className={classes.root}>
                <IconButton
                    onClick={this.handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="First Page"
                >
                    {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
                </IconButton>
                <IconButton
                    onClick={this.handleBackButtonClick}
                    disabled={page === 0}
                    aria-label="Previous Page"
                >
                    {theme.direction === "rtl" ? (
                        <KeyboardArrowRight />
                    ) : (
                        <KeyboardArrowLeft />
                    )}
                </IconButton>
                <IconButton
                    onClick={this.handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Next Page"
                >
                    {theme.direction === "rtl" ? (
                        <KeyboardArrowLeft />
                    ) : (
                        <KeyboardArrowRight />
                    )}
                </IconButton>
                <IconButton
                    onClick={this.handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Last Page"
                >
                    {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
                </IconButton>
            </div>
        );
    }
}

TablePaginationActions.propTypes = {
    classes: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    theme: PropTypes.object.isRequired
};

function html_entity_decode(texte) {

//texte = texte.replace(/#/g,'&#35;'); // 160 A0
//texte = texte.replace(/\n/g,'&#92;n'); // 160 A0
//texte = texte.replace(/\r/g,'&#92;r'); // 160 A0

    texte = texte.replace(/&amp;/g,'&'); // 38 26
    texte = texte.replace(/&quot;/g,'"'); // 34 22
    texte = texte.replace(/&lt;/g,'<'); // 60 3C
    texte = texte.replace(/&gt;/g,'>'); // 62 3E

    texte = texte.replace(/&cent;/g,'\\242');
    texte = texte.replace(/&pound;/g,'\\243');
    texte = texte.replace(/&euro;/g,'\\€');
    texte = texte.replace(/&yen;/g,'\\245');
    texte = texte.replace(/&deg;/g,'\\260');
//texte = texte.replace(/\274/g,'&frac14;');
    texte = texte.replace(/&OElig;/g,'\\274');
//texte = texte.replace(/\275/g,'&frac12;');
    texte = texte.replace(/&oelig;/g,'\\275');
//texte = texte.replace(/\276/g,'&frac34;');
    texte = texte.replace(/&Yuml;/g,'\\276');
    texte = texte.replace(/&iexcl;/g,'\\241');
    texte = texte.replace(/&laquo;/g,'\\253');
    texte = texte.replace(/&raquo;/g,'\\273');
    texte = texte.replace(/&iquest;/g,'\\277');
    texte = texte.replace(/&Agrave;/g,'\\300');
    texte = texte.replace(/&Aacute;/g,'\\301');
    texte = texte.replace(/&Acirc;/g,'\\302');
    texte = texte.replace(/&Atilde;/g,'\\303');
    texte = texte.replace(/&Auml;/g,'\\304');
    texte = texte.replace(/&Aring;/g,'\\305');
    texte = texte.replace(/&AElig;/g,'\\306');
    texte = texte.replace(/&Ccedil;/g,'\\307');
    texte = texte.replace(/&Egrave;/g,'\\310');
    texte = texte.replace(/&Eacute;/g,'\\311');
    texte = texte.replace(/&Ecirc;/g,'\\312');
    texte = texte.replace(/&Euml;/g,'\\313');
    texte = texte.replace(/&Igrave;/g,'\\314');
    texte = texte.replace(/&Iacute;/g,'\\315');
    texte = texte.replace(/&Icirc;/g,'\\316');
    texte = texte.replace(/&Iuml;/g,'\\317');
    texte = texte.replace(/&ETH;/g,'\\320');
    texte = texte.replace(/&Ntilde;/g,'\\321');
    texte = texte.replace(/&Ograve;/g,'\\322');
    texte = texte.replace(/&Oacute;/g,'\\323');
    texte = texte.replace(/&Ocirc;/g,'\\324');
    texte = texte.replace(/&Otilde;/g,'\\325');
    texte = texte.replace(/&Ouml;/g,'\\326');
    texte = texte.replace(/&Oslash;/g,'\\330');
    texte = texte.replace(/&Ugrave;/g,'\\331');
    texte = texte.replace(/&Uacute;/g,'\\332');
    texte = texte.replace(/&Ucirc;/g,'\\333');
    texte = texte.replace(/&Uuml;/g,'\\334');
    texte = texte.replace(/&Yacute;/g,'\\335');
    texte = texte.replace(/&THORN;/g,'\\336');
    texte = texte.replace(/&szlig;/g,'\\337');
    texte = texte.replace(/&agrave;/g,'\\340');
    texte = texte.replace(/&aacute;/g,'\\341');
    texte = texte.replace(/&acirc;/g,'\\342');
    texte = texte.replace(/&atilde;/g,'\\343');
    texte = texte.replace(/&auml;/g,'\\344');
    texte = texte.replace(/&aring;/g,'\\345');
    texte = texte.replace(/&aelig;/g,'\\346');
    texte = texte.replace(/&ccedil;/g,'\\347');
    texte = texte.replace(/&egrave;/g,'\\350');
    texte = texte.replace(/&eacute;/g,'\\351');
    texte = texte.replace(/&ecirc;/g,'\\352');
    texte = texte.replace(/&euml;/g,'\\353');
    texte = texte.replace(/&igrave;/g,'\\354');
    texte = texte.replace(/&iacute;/g,'\\355');
    texte = texte.replace(/&icirc;/g,'\\356');
    texte = texte.replace(/&iuml;/g,'\\357');
    texte = texte.replace(/&eth;/g,'\\360');
    texte = texte.replace(/&ntilde;/g,'\\361');
    texte = texte.replace(/&ograve;/g,'\\362');
    texte = texte.replace(/&oacute;/g,'\\363');
    texte = texte.replace(/&ocirc;/g,'\\364');
    texte = texte.replace(/&otilde;/g,'\\365');
    texte = texte.replace(/&ouml;/g,'\\366');
    texte = texte.replace(/&oslash;/g,'\\370');
    texte = texte.replace(/&ugrave;/g,'\\371');
    texte = texte.replace(/&uacute;/g,'\\372');
    texte = texte.replace(/&ucirc;/g,'\\373');
    texte = texte.replace(/&uuml;/g,'\\374');
    texte = texte.replace(/&yacute;/g,'\\375');
    texte = texte.replace(/&thorn;/g,'\\376');
    texte = texte.replace(/&yuml;/g,'\\377');
    return texte;
}

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
    withTheme: true
})(TablePaginationActions);

const styles = theme => ({
    root: {
        width: "100%",
        marginTop: theme.spacing.unit * 3
    },
    table: {
        minWidth: 500
    },
    tableWrapper: {
        overflowX: "auto"
    },
    progress: {
        margin: theme.spacing.unit * 2,
    }
});

@observer
class CustomPaginationActionsTable extends React.Component {


    state = {

        //rows: [].sort((a, b) => (a.title_ssi < b.title_ssi ? -1 : 1)),
        rows: [],
        page: 0,
        rowsPerPage: 5,
        open : false,
        dataDoc:null,
        facettes : "",
    };

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
        document.full_text_tfr_siv = this.state.title_ssi;
    }

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleFacettes = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ page: 0, rowsPerPage: event.target.value });
    };

    affichageContenu = (event, iddoc , titre, date, extrait)=> {
        console.log('test');
        this.setState({open : true});
        this.state.rows.titre = titre;
        this.state.rows.date = date;
        fetch("http://localhost:9995/api/v1/search/text/"+iddoc, {
            method: 'GET',
            headers: {
                'Accept': "*/*",
                'Content-Type': "application/json"
            },
        }).then(response => {
            try {
                return response.json()
            } catch (e) {
                console.log(e)
            }
        }).then(data => {
            this.setState({dataDoc : data})
        });
    };

    handleClose = () => {
        this.setState({ open: false });
        this.setState({dataDoc : null});
    };


    render() {
        const { classes } = this.props;
        const rows = store.triParDate ? store.getResultsByDate : store.getResultsByPert;
        const { rowsPerPage, page } = this.state;
        const rowsFacets = store.facets;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
        const current = store.current_user;


        return (
            <Paper className={classes.root}>
               {store.current_search.term !== '' && (
                <div>
                    <Typography variant={"h4"} component={"h1"}>Résultats pour le mot <b>{store.current_search.term}</b></Typography>
                    <div className={classes.tableWrapper}>
                    <Table className={classes.table}>

                        <TableHead>
                            <TableRow>
                                <CustomTableCell>Trier par :</CustomTableCell>
                                <CustomTableCell align="right"><RadioButton/></CustomTableCell>
                                <CustomTableCell> <form className={classes.root} autoComplete="off">
                                    <FormControl className={classes.formControl}>
                                        <InputLabel htmlFor="age-simple">Facettes</InputLabel>
                                        <Select
                                            value={this.state.facettes}
                                            onChange={this.handleFacettes}
                                            inputProps={{
                                                name: 'facettes',
                                                id: 'facettes-simple',
                                            }}
                                        >

                                            <MenuItem value="">
                                                <em>Null</em>
                                            </MenuItem>

                                            {Object.keys(rowsFacets).map((keyName,i)=>(
                                                <MenuItem value={keyName}>{keyName}({rowsFacets[keyName]})
                                                </MenuItem>
                                                ))}



                                        </Select>
                                    </FormControl>
                                </form>
                                </CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell2 align="left">Titre</CustomTableCell2>
                                <CustomTableCell2 align="left">Extrait</CustomTableCell2>
                                <CustomTableCell2 align="left">Date</CustomTableCell2>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map(row => (
                                    <TableRow key={row.id} onClick={event => this.affichageContenu(event, row.iddoc, row.titre, row.date, row.extrait)}>
                                        <TableCell component="th" scope="row" align="left">{row.titre} </TableCell>
                                        <TableCell align="left">{html_entity_decode(row.extrait)}</TableCell>
                                        <TableCell align="left">{row.date} </TableCell>
                                    </TableRow>

                                ))}


                            {emptyRows > 0 && (
                                <TableRow style={{ height: 30 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 15]}
                                    colSpan={3}
                                    count={rows.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    SelectProps={{
                                        native: true
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    ActionsComponent={TablePaginationActionsWrapped}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                    </div></div>)
                    }

                    {store.current_search.term === '' && (
                    <div>
                        <Typography variant={"h4"} component={"h1"}>Entrer un terme pour commencer une recherche</Typography>
                    </div>
                )}

                    <Dialog
                        open={this.state.open}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{this.state.rows.titre +" - "+this.state.rows.date}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                { this.state.dataDoc === null && (
                                    <CircularProgress className={classes.progress} />
                                )}
                                {this.state.dataDoc}
                            </DialogContentText>
                        </DialogContent>
                    </Dialog>
            </Paper>
        );
    }
}

CustomPaginationActionsTable.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomPaginationActionsTable);
