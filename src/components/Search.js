import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { store } from "../stores/Store";
import { observer } from "mobx-react";


@observer
class Autocomplete extends Component {

    state = {
        // The active selection's index
        activeSuggestion: 0,
        // The suggestions that match the user's input
        filteredSuggestions: [],
        // Whether or not the suggestion list is shown
        showSuggestions: false,
        // What the user has entered
        userInput: "",
        suggestions:[],
    };

    onChange = e => {
        // Filter our suggestions that don't contain the user's input
        this.setState({
            userInput: e.currentTarget.value
        });

        if(e.currentTarget.value.length >= 3) {

            fetch("http://localhost:9995/api/v1/autocomplete/" + e.currentTarget.value, {
                method: 'GET',
                headers: {
                    'Accept': "*/*",
                    'Content-Type': "application/json"
                },
            }).then(response => {
                try {
                    return response.json()
                } catch (e) {
                    console.log(e)
                }
            }).then(data => {

                this.setState({suggestions: data});

                this.setState({
                    activeSuggestion: 0,
                    filteredSuggestions: this.state.suggestions,
                    showSuggestions: true
                });
            });
        }
    };

    onClick = e => {
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: e.currentTarget.innerText
        });

        console.log(new Date(store.current_search.dateDebut).toISOString());
        console.log(new Date(store.current_search.dateFin).toISOString());
        store.current_search.term = e.currentTarget.innerText;
        console.log("Click");
        fetch("http://localhost:9995/api/v1/search", {
            method:'POST',
            headers: {
                'Content-Type': "application/json",
                'Authorization':store.current_user.token
            },
            body: JSON.stringify({
                term:store.current_search.term,
                dateStart:new Date(store.current_search.dateDebut).toISOString(),
                dateEnd:new Date(store.current_search.dateFin).toISOString()
            })}).then(response => {
            try {
                return response.json()
            } catch (e) {
                console.log(e)
            }
        }).then(data => {
            console.log("Ayé");
            store.results = data['results'];
            store.facets=data['facets'];

            console.log(data['facets']);
            store.addHistorique(data['history'])
            console.log("Ayé2");
        });
    };

    onKeyDown = e => {
        const { activeSuggestion, filteredSuggestions } = this.state;
        // User pressed the enter key
        if (e.keyCode === 13) {
            console.log(new Date(store.current_search.dateDebut).toISOString());
            console.log(new Date(store.current_search.dateFin).toISOString());

            this.setState({
                activeSuggestion: 0,
                showSuggestions: false,
                userInput: filteredSuggestions[activeSuggestion]
            });
            store.current_search.term = filteredSuggestions[activeSuggestion];
            fetch("http://localhost:9995/api/v1/search", {
                method:'POST',
                headers: {
                    'Content-Type': "application/json",
                    'Authorization':store.current_user.token
                },
                body: JSON.stringify({
                    term:store.current_search.term,
                    dateStart:new Date(store.current_search.dateDebut).toISOString(),
                    dateEnd:new Date(store.current_search.dateFin).toISOString()
                })}).then(response => {
                    try {
                        return response.json()
                    } catch (e) {
                        console.log(e)
                    }
                }).then(data => {
                    store.results = data['results'];
                    store.facets=data['facets'];
                    store.addHistorique(data['history'])
                });
        }
        // User pressed the up arrow
        else if (e.keyCode === 38) {
            if (activeSuggestion === 0) {
                return;
            }

            this.setState({ activeSuggestion: activeSuggestion - 1 });
        }
        // User pressed the down arrow
        else if (e.keyCode === 40) {
            if (activeSuggestion - 1 === filteredSuggestions.length) {
                return;
            }
            this.setState({ activeSuggestion: activeSuggestion + 1 });
        }
    };

    render() {
        const {
            onChange,
            onClick,
            onKeyDown,
            state: {
                activeSuggestion,
                filteredSuggestions,
                showSuggestions,
                userInput
            }
        } = this;

        let suggestionsListComponent;

        if (showSuggestions && userInput) {
            if (filteredSuggestions.length) {
                suggestionsListComponent = (
                    <ul className="suggestions">
                        {filteredSuggestions.map((suggestion, index) => {
                            let className;

                            // Flag the active suggestion with a class
                            if (index === activeSuggestion) {
                                className = "suggestion-active";
                            }

                            return (
                                <li className={className} key={suggestion} onClick={onClick}>
                                    {suggestion}
                                </li>
                            );
                        })}
                    </ul>
                );
            } else {
                suggestionsListComponent = (
                    <div className="no-suggestions">
                        <em>No suggestions, you're on your own!</em>
                    </div>
                );
            }
        }

        return (
            <Fragment>
                <input
                    type="text"
                    onChange={onChange}
                    onKeyDown={onKeyDown}
                    value={userInput}
                />
                {suggestionsListComponent}
            </Fragment>
        );
    }
}

export default Autocomplete;
