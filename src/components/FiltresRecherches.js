import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Slider from 'rc-slider';
import TextField from '@material-ui/core/TextField';
import 'rc-slider/assets/index.css';
import MenuItem from '@material-ui/core/MenuItem';
import IntegrationReactSelect from './Search.js';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { store } from "../stores/Store";
import { observer } from "mobx-react";
import Popover from "@material-ui/core/Popover";
import MenuList from "@material-ui/core/MenuList";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Icon from '@material-ui/core/Icon';


const Range = Slider.Range;

const style = {  margin: 50, display : 'flex'  };



import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import RetractableButton from "./RetractableButton";

const styles = theme => ({
    container: {
        display: 'flex',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 50,
    },

    root: {
        width: '100%',
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit * 3,
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft : 5,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 300,
            height : 25,
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },


});



class BarSearchFiltres extends React.Component
{
    state = {
        anchorEl: null,
        mobileMoreAnchorEl: null,
        min:1500,
        max:2019,
        tranche:[],
        open:false
    };


    log = (value) => {
        console.log(value); //eslint-disable-line
        this.setState({ min : value[0] });
        this.setState({ max : value[1] });
    };


    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    handleProfileMenuOpen = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuClose = () => {
        this.setState({ anchorEl: null });
        this.handleMobileMenuClose();
    };

    handleLogout = (event) => {
        store.current_user = null;
        store.current_search = {term:'',dateDebut:'1500',dateFin:'2019'};
    };

    handleToggle = () => {
        this.setState(state => ({ open: !state.open }));
    };

    render() {
        const { anchorEl, open } = this.state;
        const { classes } = this.props;
        const current = store.current_user;

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>

                        <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                            Le journal 2.0
                        </Typography>
                        <div className={classes.search}>
                            <IntegrationReactSelect
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput,
                                }}

                            />


                        </div>
                        <FiltresRecherches classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        />
                        <div className={classes.grow} />

                        <div className={classes.sectionDesktop}>
                            <RetractableButton name={store.current_user.name} icon={"person"} buttonRef={node => {this.state.anchorEl = node;}} onClick={this.handleToggle} color={''}/>

                            { current != null && current !== undefined && current !== 'undefined' && (
                                <Popover open={open} anchorOrigin={{vertical: 'bottom', horizontal: 'center'}} transformOrigin={{vertical: 'top', horizontal: 'center'}} anchorEl={anchorEl}>
                                    <ClickAwayListener onClickAway={this.handleToggle}>
                                        <MenuList>
                                            <MenuItem disabled={true} dense={true}>
                                                <Typography variant="overline">
                                                    {current.role}
                                                </Typography>
                                            </MenuItem>
                                            <MenuItem onClick={this.handleLogout}>
                                                <ListItemIcon style={{color:'red'}}><Icon>exit_to_app</Icon></ListItemIcon>
                                                <ListItemText inset primary="Se déconnecter" />
                                            </MenuItem>

                                        </MenuList>
                                    </ClickAwayListener>
                                </Popover>
                            )}
                        </div>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

@observer
class FiltresRecherches extends React.Component {

    state = {
        min:1500,
        max:2019,
        tranche:[],
        dateMin : new Date(),
        dateMax : new Date(),
    };


     log = (value) => {
        console.log(value); //eslint-disable-line
        this.setState({ min : value[0] });
        this.setState({ max : value[1] });
        console.log(this.state.date);
    };


    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
};



    render() {

        const { classes } = this.props;
        this.state.tranche=[this.state.min, this.state.max];
        this.state.dateMin.setFullYear(this.state.min,1,1);
        store.current_search.dateDebut=this.state.dateMin;

        this.state.dateMax.setFullYear(this.state.max,1,1);
        store.current_search.dateFin=this.state.dateMax;

            return (
                <div>


                    <form style={{display : 'flex'}} className={classes.container} noValidate autoComplete="off">
                    <TextField
                        id="filled-number"
                        label="Début"
                        value={this.state.min}
                        onChange={this.handleChange('min')}
                        type="number"
                        className={classes.textField}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                        variant="standard"
                    />

                        <Range style = {{marginTop: 35,marginLeft: 15,marginRight : 15}} step={1} value ={this.state.tranche}  defaultValue={[1500, 2019]} min={1500} max = {2019} onChange={this.log} />

                        <TextField
                        id="filled-number"
                        label="Fin"
                        value={this.state.max}
                        onChange={this.handleChange('max')}
                        type="number"
                        className={classes.textField}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                        variant="standard"
                    />
                    </form>


                     </div>
        );
    }
}


FiltresRecherches.propTypes = {
    classes: PropTypes.object.isRequired,
};

BarSearchFiltres.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BarSearchFiltres);