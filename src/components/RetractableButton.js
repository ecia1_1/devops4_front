import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';

import Media from 'react-media';
import {Icon} from "@material-ui/core";

const styles = theme => ({
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
});

class RetractableButton extends React.Component {
    render() {
        const { classes } = this.props;

        return (
                <Media query="(max-width: 799px)">
                    {matches =>
                        matches ? (
                            <Fab aria-label="Espace personnel" color={this.props.color == null ? "primary" : this.props.color} onClick={this.props.onClick} buttonRef={this.props.buttonRef} style={this.props.style}>
                                <Icon>{this.props.icon}</Icon>
                            </Fab>
                        ) : (
                            <Fab variant={"extended"} aria-label="Espace personnel" color={this.props.color == null ? "primary" : this.props.color} onClick={this.props.onClick} buttonRef={this.props.buttonRef} style={{minWidth:this.props.minWidth,width:this.props.fixedWidth,...this.props.style}}>
                                <Icon className={classes.extendedIcon}>{this.props.icon}</Icon>
                                {this.props.name}
                            </Fab>
                        )
                    }

                </Media>
        );
    }
}

RetractableButton.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RetractableButton);
