import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import HistoryIcon from "@material-ui/icons/History";
import { store } from "../stores/Store";
import { observer } from "mobx-react";


const drawerWidth = 240;

const styles = theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0
    },
    drawerPaper: {
        width: drawerWidth
    },
    toolbar: theme.mixins.toolbar
});

@observer
class Historique extends React.Component {





    rechercher=(event,current_titre,current_dateStart,current_dateEnd)=>{



        var dateDebut = new Date();
        dateDebut.setFullYear(current_dateStart,1,1);

        var dateFin = new Date();
        dateFin.setFullYear(current_dateEnd,1,1);

        fetch("http://localhost:9995/api/v1/search", {
            method:'POST',
            headers: {
                'Accept': "*/*",
                'Content-Type': "application/json",
                'Authorization':store.current_user.token
            },
            body: JSON.stringify({
                term:current_titre,
                dateStart:dateDebut.toISOString(),
                dateEnd:dateFin.toISOString(),
            })}).then(response => {
            try {
                return response.json()
            } catch (e) {
                console.log(e)
            }
        }).then(data => {
            store.current_search.term = current_titre;
            store.results = data['results'];
            store.facets=data['facets'];
            store.addHistorique(data['history']);
        });
    };

    componentDidMount() {
        fetch("http://localhost:9995/api/v1/history", {
            method: 'GET',
            headers: {
                'Accept': "*/*",
                'Content-Type': "application/json",
                'Authorization':store.current_user.token
            },
        }).then(response => {
            try {
                return response.json()
            } catch (e) {
                console.log(e)
            }
        }).then(data => {
            store.loadHistoriques(data)
        });
    }

    render() {
        const {classes} = this.props;

        return (
            <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper
                }}
            >
                <div className={classes.toolbar}/>
                <List/>
                <ListItem>
                    <ListItemIcon>
                        <HistoryIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Historique"/>
                </ListItem>
                <List>
                    {store.getHistoriques.map((text,index) => (
                        <ListItem button key={index} onClick={event =>this.rechercher(event,text.titre,text.dateStart,text.dateEnd)} >
                            <ListItemText primary={text.titre} secondary={text.dateStart + " - " + text.dateEnd}/>
                        </ListItem>
                    ))}
                </List>
            </Drawer>

        );
    }
}

Historique.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Historique);