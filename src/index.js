import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import App from "./components/App";
import { MomentUtils } from 'moment';
import { DateFnsUtils } from '@date-io/date-fns';
import { LuxonUtils } from '@date-io/luxon';

const CURRENT_USER_TOKEN = 'eyJpc3MiOiJ0b3B0YWwuY29tIiwiZXhwIjoxNDI2NDIwODAwLCJodHRwOi8vdG9wdGFsLmNvbS9qd3RfY2xhaW1zL2lzX2FkbWluIjp0cnVlLCJjb21wYW55IjoiVG9wdGFsIiwiYXdlc29tZSI6dHJ1ZX0';

ReactDOM.render(

    <div>
        <App />
    </div>
    , document.querySelector('#root'));
