import mobx, {autorun, computed, observable, action} from "mobx"

let counter = 0;
function createData(iddoc, titre , extrait, date) {
    counter += 1;
    return { id: counter, iddoc, titre, extrait, date};
}

class Store {
    @observable current_user = null;
    @observable historiques = [];
    @observable results = [];
    @observable params = [];
    @observable facets={};
    @observable current_search = {term:'',dateDebut:'1500',dateFin:'2019'};

    @observable triParDate = false;

    @action addHistorique (item) {
        const parsed = JSON.parse(item);
        parsed.dateStart = new Date(parsed.dateStart).getFullYear();
        parsed.dateEnd = new Date(parsed.dateEnd).getFullYear();
        this.historiques.unshift(parsed)
    }

    @action loadHistoriques (items) {
        const res = [];
        items.map((item) => {
            item.dateStart = new Date(item.dateStart).getFullYear();
            item.dateEnd = new Date(item.dateEnd).getFullYear();
            res.push(item)
        });
        this.historiques = res;
    }

    @computed get getHistoriques () {
        return this.historiques.length > 0 ? this.historiques : []
    }

    @computed get getResultsByPert() {
        let resultats = [];
        this.results.map((result) =>
            resultats.push(createData(result.id, result.title_ssi, result.full_text_tfr_siv, new Date(result.date_dtsi).toLocaleDateString()))
        );
        return resultats;
    }

    @computed get getResultsByDate() {
        let res_store = this.results;
        res_store = res_store.slice().sort((a,b) => new Date(b.date_dtsi) - new Date(a.date_dtsi));
        let resultats = [];
        res_store.map((result) =>
            resultats.push(createData(result.id, result.title_ssi, result.full_text_tfr_siv, new Date(result.date_dtsi).toLocaleDateString()))
        );
        return resultats;
    }
}

let store = new Store();
global.store = store;

export {store};
