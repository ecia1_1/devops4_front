const fs = require('fs');
var sec_test = [];

fs.readdirSync('./docs/api/').forEach(file => {
    var filename = file.replace(/\..+$/, '');
    filename = filename.charAt(0).toUpperCase() + filename.slice(1);
    sec_test.push({name:filename,content:'docs/api/'+file});
});

module.exports = {
    sections: [
        {
            name: 'Introduction',
            content: 'docs/intro.md'
        },
        {
            name: 'Démarrage',
            sections: [
                {
                    name: 'Installation',
                    content: 'docs/install.md',
                    description: 'The description for the installation section'
                },
                {
                    name: 'Configuration',
                    content: 'docs/config.md'
                }
            ]
        },
        {
            name: 'Components',
            content: 'docs/ui.md',
            components: '/src/components/**/*.js',
            exampleMode: 'expand', // 'hide' | 'collapse' | 'expand'
            usageMode: 'expand' // 'hide' | 'collapse' | 'expand'
        },
        {
            name: 'API',
            content: 'docs/api.md',
            sections: sec_test
        }
    ]
};